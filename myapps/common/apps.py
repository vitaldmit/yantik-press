from django.apps import AppConfig


class CommonConfig(AppConfig):
    name = 'myapps.common'
