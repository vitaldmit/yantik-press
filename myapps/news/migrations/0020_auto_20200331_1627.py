# Generated by Django 3.0.3 on 2020-03-31 13:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0019_auto_20200331_1556'),
    ]

    operations = [
        migrations.AlterField(
            model_name='banners',
            name='type',
            field=models.CharField(choices=[('with_image', 'Графический'), ('with_text', 'Текстовый'), ('with_image_text', 'Текстовый')], default='image', max_length=15, verbose_name='Тип баннера'),
        ),
    ]
