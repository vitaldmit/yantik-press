# Generated by Django 3.0.3 on 2020-05-13 07:45

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import tinymce.models


class Migration(migrations.Migration):

    dependencies = [
        ('news', '0037_videogallery_content'),
    ]

    operations = [
        migrations.CreateModel(
            name='VideoNews',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200, verbose_name='Заголовок')),
                ('slug', models.SlugField(max_length=200, unique_for_date='publish', verbose_name='ЧПУ')),
                ('video', models.FileField(blank=True, null=True, upload_to='newsvideos/%Y/%m/%d/', verbose_name='Видео')),
                ('cap', models.CharField(blank=True, max_length=150, null=True, verbose_name='Видео CAP.RU')),
                ('youtube', models.CharField(blank=True, help_text='КОД ВИДЕО', max_length=100, null=True, verbose_name='Видео YouTube')),
                ('content', tinymce.models.HTMLField(blank=True, null=True, verbose_name='Контент')),
                ('visible', models.BooleanField(default=1, verbose_name='Показывать')),
                ('source', models.CharField(blank=True, max_length=150, null=True, verbose_name='Первоисточник')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Создан')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Обновлен')),
                ('publish', models.DateTimeField(default=django.utils.timezone.now, help_text='Дата и время публикации', verbose_name='Дата публикации')),
                ('news', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='videonews', to='news.News', verbose_name='Связанная новость')),
            ],
            options={
                'verbose_name': 'Видеоновости',
                'verbose_name_plural': 'Видеоновости',
                'ordering': ('-created',),
            },
        ),
        migrations.DeleteModel(
            name='VideoGallery',
        ),
    ]
