from django.apps import AppConfig


class NewsConfig(AppConfig):
    name = 'myapps.news'
    verbose_name = "Пресс-центр"
