from django.apps import AppConfig


class AboutConfig(AppConfig):
    name = 'myapps.about'
    verbose_name = "Редакция"
