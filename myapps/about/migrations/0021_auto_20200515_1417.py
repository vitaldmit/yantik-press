# Generated by Django 3.0.3 on 2020-05-15 11:17

from django.db import migrations, models
import myapps.about.models


class Migration(migrations.Migration):

    dependencies = [
        ('about', '0020_auto_20200515_1145'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documentsfiles',
            name='file',
            field=models.FileField(upload_to=myapps.about.models.doc_dir_path, verbose_name='Документ'),
        ),
    ]
