# Generated by Django 3.0.3 on 2020-04-03 16:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('about', '0014_auto_20200331_1448'),
    ]

    operations = [
        migrations.AddField(
            model_name='contacts',
            name='map',
            field=models.TextField(blank=True, null=True),
        ),
    ]
